<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    if(auth()->user()){
        $rol = auth()->user()->rol;
        switch ($rol) {
            case 'usuario':
                return redirect()->route('home-usuario');
                break;
            case 'administrador':
                return redirect()->route('home-administrador');
                break;
            case 'restaurante':
                return redirect()->route('home-restaurante');
                break;
            case 'root':
                return redirect()->route('home-root');
                break;
            
            default:
                if(auth()->check())
                    auth()->logout();
                return redirect()->route('login');
                break;
        }
    }

    return view('welcome');
});

Route::get('auth/logout', 'Auth\LoginController@logout')->name('logout2');

Auth::routes();

/*
|--------------------------------------------------------------------------
| Usuario básico
|--------------------------------------------------------------------------
*/
Route::middleware(['usuario'])->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/usuario', 'UsuarioController@index')->name('home-usuario');
});

/*
|--------------------------------------------------------------------------
| Usuario administrador
|--------------------------------------------------------------------------
*/
Route::prefix('admin')->middleware(['administrador'])->group(function () {
    Route::get('/dashboard', 'AdministradorController@dashboard')->name('home-administrador');
});

/*
|--------------------------------------------------------------------------
| Usuario root
|--------------------------------------------------------------------------
*/
Route::middleware(['root'])->group(function () {
    Route::get('/root', 'RootController@index')->name('home-root');
});

/*
|--------------------------------------------------------------------------
| Usuario restaurante
|--------------------------------------------------------------------------
*/
Route::prefix('restaurante')->middleware(['restaurante'])->group(function () {
    Route::get('/dashboard', 'RestauranteController@index')->name('home-restaurante');
    
    Route::get('/categorias', 'RestauranteController@categorias')->name('categorias-restaurante');
    Route::post('/categorias/guardar', 'RestauranteController@guardarCategorias')->name('categorias-restaurante-guardar');
    Route::get('/categorias/editar/{id}', 'RestauranteController@editarCategorias')->name('categorias-restaurante-editar');
    Route::post('/categorias/actualizar/{id}', 'RestauranteController@actualizarCategorias')->name('categorias-restaurante-actualizar');

    Route::get('/productos', 'RestauranteController@productos')->name('productos-restaurante');
    Route::post('/productos/guardar', 'RestauranteController@guardarProductos')->name('productos-restaurante-guardar');
    Route::get('/productos/editar/{id}', 'RestauranteController@editarProductos')->name('productos-restaurante-editar');
    Route::post('/productos/actualizar/{id}', 'RestauranteController@actualizarProductos')->name('productos-restaurante-actualizar');

});

/*
|--------------------------------------------------------------------------
| Rutas de pruebas
|--------------------------------------------------------------------------
*/
Route::get('plantilla', function(){
    return view('plantilla');
});

