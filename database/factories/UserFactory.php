<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => 'root@root.com',
        'email_verified_at' => now(),
        'rol' => 'root',
        'password' => bcrypt('12345678'), // password
        'remember_token' => Str::random(10),
    ];
});

$factory->state(User::class, 'administrador', function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => 'admin@admin.com',
        'email_verified_at' => now(),
        'rol' => 'administrador',
        'password' => bcrypt('12345678'), // password
        'remember_token' => Str::random(10),
    ];
});

$factory->state(User::class, 'restaurante', function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => 'demo@demo.com',
        'email_verified_at' => now(),
        'rol' => 'restaurante',
        'password' => bcrypt('12345678'), // password
        'remember_token' => Str::random(10),
    ];
});
