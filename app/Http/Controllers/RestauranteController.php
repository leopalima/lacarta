<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Validator;
use Image;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

use App\Categoria;
use App\Producto;

class RestauranteController extends Controller
{
    public function index()
    {
        $data = [
        'totalCategorias' => Categoria::where('user_id', Auth::user()->id)->count(),
        'totalProductos' => Producto::where('user_id', Auth::user()->id)->count(),
        ];
        return view('restaurante.dashboard', compact('data'));
    }

    public function categorias()
    {
        $categorias = Categoria::where('user_id', Auth::user()->id)->get();

        return view('restaurante.categorias.categorias-index', compact('categorias'));
    }

    public function guardarCategorias(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'txtnombre' => 'required',
            'txtdescripcion' => 'required',
            'txtestatus' => 'required|integer',
            'txtordenamiento' => 'required|integer',
        ], [
            'txtnombre.required' => 'Se requiere el nombre de la categoría',
            'txtdescripcion.required' => 'Se requiere la descripción de la categoría',
            'txtestatus.required' => 'Debe seleccionar un estatus',
            'txtordenamiento.required' => 'Indique el orden en el listado',
        ]);

        if ($validator->fails()) {
            $respuesta = [
                'error' => 'error',
                'mensaje' => $validator->errors()->first(),
                'input' => $validator->errors(),
            ];

        }else{
            try {
                $categoria = new Categoria;
                $categoria->nombre = $request->txtnombre;
                $categoria->descripcion = $request->txtdescripcion;

                if (!Storage::disk('public')->exists('categoria')) {
                    Storage::disk('public')->makeDirectory('categoria');
                }

                if ($request->hasFile('txtfoto')) {
                    $photo = $request->file('txtfoto');
                    $filename = time() . Str::random(10) . '.' . strtolower($photo->getClientOriginalExtension());
                    Image::make($photo)->resize(200, 200)->save('storage/categoria/' . $filename);
                    $categoria->foto = $filename;
                }

                $categoria->estatus = $request->txtestatus;
                $categoria->ordenamiento = $request->txtordenamiento;
                $categoria->user_id = Auth::user()->id;
                $categoria->save();

                $respuesta = [
                    "url" => route('categorias-restaurante')
                ];
            } catch (\Exception $e) {
                $respuesta = [
                    'error' => 'error',
                    'mensaje' => "Error al guardar el registro." . $e,
                ];
            }
        }



        return response()->json($respuesta);
    }

    public function editarCategorias($id)
    {
        try {
            $categorias = Categoria::all()->count();
            $categoria = Categoria::findOrFail($id);
        } catch (\Exception $e) {
            return abort(404);
        }

        return view('restaurante.categorias.categorias-editar', compact('categoria', 'categorias'));
        return response()->json($categoria);
    }

    public function actualizarCategorias(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'txtnombre' => 'required',
            'txtdescripcion' => 'required',
            'txtestatus' => 'required|integer',
            'txtordenamiento' => 'required|integer',
        ], [
            'txtnombre.required' => 'Se requiere el nombre de la categoría',
            'txtdescripcion.required' => 'Se requiere la descripción de la categoría',
            'txtestatus.required' => 'Debe seleccionar un estatus',
            'txtordenamiento.required' => 'Indique el orden en el listado',
        ]);

        if ($validator->fails()) {
            $respuesta = [
                'error' => 'error',
                'mensaje' => $validator->errors()->first(),
                'input' => $validator->errors(),
            ];

        }else{

            try {

                $categoria = Categoria::findOrFail($id);
                $categoria->nombre = $request->txtnombre;
                $categoria->descripcion = $request->txtdescripcion;


                if (!Storage::disk('public')->exists('categoria')) {
                    Storage::disk('public')->makeDirectory('categoria');
                }

                if ($request->hasFile('txtfoto')) {
                    $photo = $request->file('txtfoto');
                    $filename = time() . Str::random(10) . '.' . strtolower($photo->getClientOriginalExtension());
                    Image::make($photo)->resize(200, 200)->save('storage/categoria/' . $filename);
                    $categoria->foto = $filename;
                }

                $categoria->estatus = $request->txtestatus;
                $categoria->ordenamiento = $request->txtordenamiento;
                $categoria->user_id = Auth::user()->id;
                $categoria->save();

                $respuesta = [
                    "url" => route('categorias-restaurante')
                ];
            } catch (\Exception $e) {
                $respuesta = [
                    'error' => 'error',
                    'mensaje' => $e->getmessage(),
                ];
            }
        }



        return response()->json($respuesta);
    }

    public function productos()
    {
        $productos = Producto::where('user_id', Auth::user()->id)->get();
        $categorias = Categoria::where('user_id', Auth::user()->id)->get();

        return view('restaurante.productos.productos-index', compact('productos','categorias'));
    }

    public function guardarProductos(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'txtnombre' => 'required',
            'txtdescripcion' => 'required',
            'txtalerjenos' => 'required',
            'txtfoto' => 'required',
            'txtestatus' => 'required',
            'txtprecio' => 'required',
            'txtordenamiento' => 'required',
            'txtdestacado' => 'required',
            'txtcategoria' => 'required',
        ], [
            'txtnombre.required' => 'El campo nombre es requerido.',
            'txtdescripcion.required' => 'El campo descripcion es requerido.',
            'txtalerjenos.required' => 'El campo alerjenos es requerido.',
            'txtfoto.required' => 'El campo foto es requerido.',
            'txtestatus.required' => 'El campo estatus es requerido.',
            'txtprecio.required' => 'El campo precio es requerido.',
            'txtordenamiento.required' => 'El campo ordenamiento es requerido.',
            'txtdestacado.required' => 'El campo destacado es requerido.',
            'txtcategoria.required' => 'El campo categoria es requerido.',
        ]);

        if ($validator->fails()) {
            $respuesta = [
                'error' => 'error',
                'mensaje' => $validator->errors()->first(),
                'input' => $validator->errors(),
            ];

        }else{

            try {

                $producto = new Producto;
                $producto->nombre = $request->txtnombre;
                $producto->descripcion = $request->txtdescripcion;
                $producto->alerjenos = $request->txtalerjenos;

                if (!Storage::disk('public')->exists('producto')) {
                    Storage::disk('public')->makeDirectory('producto');
                }

                if ($request->hasFile('txtfoto')) {
                    $photo = $request->file('txtfoto');
                    $filename = time() . Str::random(10) . '.' . strtolower($photo->getClientOriginalExtension());
                    Image::make($photo)->resize(200, 200)->save('storage/producto/' . $filename);
                    $producto->foto = $filename;
                }

                $producto->estatus = $request->txtestatus;
                $producto->precio = $request->txtprecio;
                $producto->ordenamiento = $request->txtordenamiento;
                $producto->destacado = $request->txtdestacado;
                $producto->user_id = $request->txtuser_id;
                $producto->categoria_id = $request->txtcategoria;
                $producto->user_id = Auth::user()->id;
                $producto->save();

                            

                $respuesta = [
                    "url" => route('productos-restaurante')
                ];
            } catch (\Exception $e) {
                $respuesta = [
                    'error' => 'error',
                    'mensaje' => "Error al guardar el registro.",
                ];
            }
        }



        return response()->json($respuesta);
    }

    public function editarProductos($id)
    {
        try {
            $totalproductos = Producto::all()->count();
            $producto = Producto::findOrFail($id);
            $categorias = Categoria::where('user_id', Auth::user()->id)->get();
        } catch (\Exception $e) {
            return abort(404);
        }

        return view('restaurante.productos.productos-editar', compact('producto', 'categorias', 'totalproductos'));
        return response()->json($categoria);
    }

    public function actualizarProductos(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'txtnombre' => 'required',
            'txtdescripcion' => 'required',
            'txtalerjenos' => 'required',
            'txtestatus' => 'required',
            'txtprecio' => 'required',
            'txtordenamiento' => 'required',
            'txtdestacado' => 'required',
            'txtcategoria' => 'required',
        ], [
            'txtnombre.required' => 'El campo nombre es requerido.',
            'txtdescripcion.required' => 'El campo descripcion es requerido.',
            'txtalerjenos.required' => 'El campo alerjenos es requerido.',
            'txtestatus.required' => 'El campo estatus es requerido.',
            'txtprecio.required' => 'El campo precio es requerido.',
            'txtordenamiento.required' => 'El campo ordenamiento es requerido.',
            'txtdestacado.required' => 'El campo destacado es requerido.',
            'txtcategoria.required' => 'El campo categoria es requerido.',
        ]);

        if ($validator->fails()) {
            $respuesta = [
                'error' => 'error',
                'mensaje' => $validator->errors()->first(),
                'input' => $validator->errors(),
            ];

        }else{

            try {
                $producto = Producto::findOrFail($id);
                $producto->nombre = $request->txtnombre;
                $producto->descripcion = $request->txtdescripcion;
                $producto->alerjenos = $request->txtalerjenos;

                if (!Storage::disk('public')->exists('producto')) {
                    Storage::disk('public')->makeDirectory('producto');
                }

                if ($request->hasFile('txtfoto')) {
                    $photo = $request->file('txtfoto');
                    $filename = time() . Str::random(10) . '.' . strtolower($photo->getClientOriginalExtension());
                    Image::make($photo)->resize(200, 200)->save('storage/producto/' . $filename);
                    $producto->foto = $filename;
                }

                $producto->estatus = $request->txtestatus;
                $producto->precio = $request->txtprecio;
                $producto->ordenamiento = $request->txtordenamiento;
                $producto->destacado = $request->txtdestacado;
                $producto->user_id = $request->txtuser_id;
                $producto->categoria_id = $request->txtcategoria;
                $producto->user_id = Auth::user()->id;
                $producto->save();

                $respuesta = [
                    "url" => route('productos-restaurante')
                ];
            } catch (\Exception $e) {
                $respuesta = [
                    'error' => 'error',
                    'mensaje' => $e->getmessage(),
                ];
            }
        }



        return response()->json($respuesta);
    }

}
