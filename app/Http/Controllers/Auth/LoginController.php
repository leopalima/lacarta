<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticated(Request $request, User $user)
    {
        $rol = $user->rol;
        switch ($rol) {
            case 'usuario':
                return redirect()->route('home-usuario');
                break;
            case 'administrador':
                return redirect()->route('home-administrador');
                break;
            case 'restaurante':
                return redirect()->route('home-restaurante');
                break;
            case 'root':
                return redirect()->route('home-root');
                break;
            
            default:
                if(auth()->check())
                    auth()->logout();
                return redirect()->route('login');
                break;
        }
    }

    /**
     * @param Request $request
     */
    protected function loggedOut(Request $request)
    {
        return redirect()->route('get.login');
    }

    public function logout(Request $request) {
        Auth::logout();
        return redirect('/login');
    }

}
