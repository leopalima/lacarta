<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdministradorController extends Controller
{
        public function index()
    {
        return response()->json('Esta es la vista de administrador');
    }

    public function dashboard(){
        return view('admin.dashboard');
    }
}
