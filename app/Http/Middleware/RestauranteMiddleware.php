<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class RestauranteMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
                $this->auth = auth()->user() ? (auth()->user()->rol === 'restaurante') : false;

        if($this->auth === true)
            return $next($request);
        
        if(auth()->check())
            auth()->logout();

        return redirect()->route('login')->with('error', 'No tiene autorización');

    }
}
