@extends('layouts.restaurante.base')

@section('css')
@endsection

@section('breadcrumb')
                            <div class="kt-subheader   kt-grid__item" id="kt_subheader">
                                <div class="kt-container ">
                                    <div class="kt-subheader__toolbar">
                                        <div class="kt-subheader__wrapper">
                                        {{--
                                            <a href="" class="btn kt-subheader__btn-primary">
                                                 &nbsp;

                                                <!--<i class="flaticon2-calendar-1"></i>-->
                                            </a>
                                        --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
@endsection

@section('cuerpo')
                                    <div class="col-md-12">

                                        <!--begin::Portlet-->
                                        <div class="kt-portlet">
                                            <div class="kt-portlet__body kt-portlet__body--fit">
                                                <div class="row row-no-padding row-col-separator-xl">
                                                    <div class="col-md-12 col-lg-12 col-xl-4">

                                                        <!--begin:: Widgets/Stats2-1 -->
                                                        <div class="kt-widget1">
                                                            <div class="kt-widget1__item">
                                                                <div class="kt-widget1__info">
                                                                    <h3 class="kt-widget1__title">Categorias</h3>
                                                                    <span class="kt-widget1__desc">Esto es solo un ejemplo</span>
                                                                </div>
                                                                <span class="kt-widget1__number kt-font-brand">+{{$data['totalCategorias']}}</span>
                                                            </div>
                                                            <div class="kt-widget1__item">
                                                                <div class="kt-widget1__info">
                                                                    <h3 class="kt-widget1__title">Productos</h3>
                                                                    <span class="kt-widget1__desc">Esto es solo un ejemplo</span>
                                                                </div>
                                                                <span class="kt-widget1__number kt-font-danger">+{{$data['totalProductos']}}</span>
                                                            </div>
                                                            <div class="kt-widget1__item">
                                                                <div class="kt-widget1__info">
                                                                    <h3 class="kt-widget1__title">Issue Reports</h3>
                                                                    <span class="kt-widget1__desc">System bugs and issues</span>
                                                                </div>
                                                                <span class="kt-widget1__number kt-font-success">-{{rand()}}%</span>
                                                            </div>
                                                        </div>

                                                        <!--end:: Widgets/Stats2-1 -->
                                                    </div>
                                                    <div class="col-md-12 col-lg-12 col-xl-4">

                                                        <!--begin:: Widgets/Stats2-2 -->
                                                        <div class="kt-widget1">
                                                            <div class="kt-widget1__item">
                                                                <div class="kt-widget1__info">
                                                                    <h3 class="kt-widget1__title">IPO Margin</h3>
                                                                    <span class="kt-widget1__desc">Awerage IPO Margin</span>
                                                                </div>
                                                                <span class="kt-widget1__number kt-font-success">+24%</span>
                                                            </div>
                                                            <div class="kt-widget1__item">
                                                                <div class="kt-widget1__info">
                                                                    <h3 class="kt-widget1__title">Payments</h3>
                                                                    <span class="kt-widget1__desc">Yearly Expenses</span>
                                                                </div>
                                                                <span class="kt-widget1__number kt-font-info">+$560,800</span>
                                                            </div>
                                                            <div class="kt-widget1__item">
                                                                <div class="kt-widget1__info">
                                                                    <h3 class="kt-widget1__title">Logistics</h3>
                                                                    <span class="kt-widget1__desc">Overall Regional Logistics</span>
                                                                </div>
                                                                <span class="kt-widget1__number kt-font-warning">-10%</span>
                                                            </div>
                                                        </div>

                                                        <!--end:: Widgets/Stats2-2 -->
                                                    </div>
                                                    <div class="col-md-12 col-lg-12 col-xl-4">

                                                        <!--begin:: Widgets/Stats2-3 -->
                                                        <div class="kt-widget1">
                                                            <div class="kt-widget1__item">
                                                                <div class="kt-widget1__info">
                                                                    <h3 class="kt-widget1__title">Orders</h3>
                                                                    <span class="kt-widget1__desc">Awerage Weekly Orders</span>
                                                                </div>
                                                                <span class="kt-widget1__number kt-font-success">+15%</span>
                                                            </div>
                                                            <div class="kt-widget1__item">
                                                                <div class="kt-widget1__info">
                                                                    <h3 class="kt-widget1__title">Transactions</h3>
                                                                    <span class="kt-widget1__desc">Daily Transaction Increase</span>
                                                                </div>
                                                                <span class="kt-widget1__number kt-font-danger">+80%</span>
                                                            </div>
                                                            <div class="kt-widget1__item">
                                                                <div class="kt-widget1__info">
                                                                    <h3 class="kt-widget1__title">Revenue</h3>
                                                                    <span class="kt-widget1__desc">Overall Revenue Increase</span>
                                                                </div>
                                                                <span class="kt-widget1__number kt-font-primary">+60%</span>
                                                            </div>
                                                        </div>

                                                        <!--end:: Widgets/Stats2-3 -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end::Portlet-->
                                    </div>
@endsection
@section('javascript')
@endsection