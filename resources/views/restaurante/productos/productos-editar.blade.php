@extends('layouts.restaurante.base')
@section('css')
@endsection

@section('breadcrumb')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <!-- <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal">Nueva Categoría</button> -->
            </div>
        </div>
    </div>
</div>
@endsection


@section('cuerpo')
<div class="col-md-12">

    <!--begin::Portlet-->
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Editar Producto
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin::Section-->
            <div class="kt-section">
                <div class="kt-section__content">
                    <form class="kt-form kt-form--label-right" method="POST" enctype="multipart/form-data" action="{{route('productos-restaurante-actualizar', ['id' => $producto->id])}}">
                        <div class="kt-portlet__body">
                            <div class="form-group row">
                                <label for="example-text-input" class="col-2 col-form-label">nombre</label>
                                <div class="col-10">
                                    <input class="form-control" type="text" value="{{$producto->nombre}}" id="txtnombre" name="txtnombre">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-2 col-form-label">Descripción</label>
                                <div class="col-10">
                                    <input class="form-control" type="text" value="{{$producto->descripcion}}" id="txtdescripcion" name="txtdescripcion">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-2 col-form-label">Alerjenos</label>
                                <div class="col-10">
                                    <input class="form-control" type="text" value="{{$producto->alerjenos}}" id="txtalerjenos" name="txtalerjenos">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-2 col-form-label">Foto</label>
                                <div class="col-10">
                                    <input class="form-control" type="file" value="" id="txtfoto" name="txtfoto" accept="image/*">
                                    <div id="txtfotoPreview"><img src="{{Storage::url('producto/' . $producto->foto)}}" alt=""></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-2 col-form-label">Precio</label>
                                <div class="col-10">
                                    <input class="form-control" type="text" value="{{$producto->precio}}" id="txtprecio" name="txtprecio">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-2 col-form-label">Destacado</label>
                                <div class="col-10">
                                    <select class="form-control" name="txtdestacado" id="txtdestacado">
                                        <option value="1" {{($producto->destacado == 1 ? "selected" : "")}}>Destacar</option>
                                        <option value="0" {{($producto->destacado == 0 ? "selected" : "")}}>Sin destacar</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-2 col-form-label">Estatus</label>
                                <div class="col-10">
                                    <select class="form-control" name="txtestatus" id="txtestatus">
                                        <option value="1">Mostrar</option>
                                        <option value="0">ocultar</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-2 col-form-label">Categoría</label>
                                <div class="col-10">
                                    <select class="form-control" name="txtcategoria" id="txtcategoria">
                                        @foreach($categorias as $categoria)
                                        <option value="{{$categoria->id}}">{{$categoria->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-2 col-form-label">Ordenamiento</label>
                                <div class="col-10">
                                    <select class="form-control" name="txtordenamiento" id="txtordenamiento">
                                        @for($i=1; $i <= $totalproductos; $i++)
                                        <option value="{{$i}}" {{($producto->ordenamiento == $i) ? "selected" : ""}}>{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                        </div>
                        @csrf
                    </form>
                    <div class="modal-footer">
                        <div class="invalid-feedback">cdfsd</div>
                        <a href="{{route('productos-restaurante')}}" type="button" class="btn btn-secondary">Cancelar</a>
                        <button type="button" class="btn btn-success" id="btnguardar">Actualizar</button>
                    </div>
                </div>
            </div>
            <!--end::Section-->
        </div>
        <!--end::Form-->
    </div>
    <!--end::Portlet-->
</div>

@endsection
@section('javascript')
<script>
$( document ).ready(function() {
    $("#btnguardar").click(function(){
        // $(this).attr("disabled", true);
        var frm = $(".kt-form");
        guardar(frm);
    });

    function guardar(formulario){
        $data = new FormData(formulario[0]);
        $url = formulario.attr("action");
        $(".invalid-feedback").html("");

        $.ajax({
            method: "POST",
            url: $url,
            data: $data,
            enctype: 'multipart/form-data',
            processData: false,  // tell jQuery not to process the data
            contentType: false   // tell jQuery not to set contentType
            })
            .done(function( data ) {
                console.log(data);
                if(data.error){
                    $(".invalid-feedback").show();
                    $(".invalid-feedback").html("<h5>" + data.mensaje+ "</h5>");
                }else{
                    window.location.href = data.url;
                }
                
            });
    }

    $("#txtfoto").change(function () {
        filePreview(this);
    });

    function filePreview(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                // $('#uploadForm + img').remove();
                $('#txtfotoPreview').html('<img src="'+e.target.result+'" width="200" height="200"/>');
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
});
</script>
@endsection