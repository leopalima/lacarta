@extends('layouts.restaurante.base')
@section('css')
@endsection

@section('breadcrumb')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal">Nuevo Producto</button>
            </div>
        </div>
    </div>
</div>
@endsection


@section('cuerpo')
<div class="col-xl-6">

    <!--begin::Portlet-->
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Lista de Productos
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin::Section-->
            <div class="kt-section">
                <div class="kt-section__content">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>Categoria</th>
                                    <th>Precio</th>
                                    <th>Destacado</th>
                                    <th>Estatus</th>
                                    <th>Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($productos as $producto)
                                <tr>
                                    <th scope="row">{{Str::limit($producto->id, 8, '')}}</th>
                                    <td>{{$producto->nombre}}</td>
                                    <td>{{$producto->categoria->nombre}}</td>
                                    <td>{{$producto->precio}}</td>
                                    <td>{{$producto->destacado}}</td>
                                    <td>{{$producto->estatus}}</td>
                                    <td><a href="{{route('productos-restaurante-editar', ['id' => $producto->id])}}" class="btn btn-warning">Editar</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <!--end::Section-->
        </div>

        <!--end::Form-->
    </div>

    <!--end::Portlet-->

</div>
<div class="modal modal-stick-to-bottom fade" id="modal"  data-backdrop="static" role="dialog" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Nuevo producto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form class="kt-form kt-form--label-right" method="POST" enctype="multipart/form-data" action="{{route('productos-restaurante-guardar')}}">
                    <div class="kt-portlet__body">
                        <div class="form-group row">
                            <label for="example-text-input" class="col-2 col-form-label">nombre</label>
                            <div class="col-10">
                                <input class="form-control" type="text" value="" id="txtnombre" name="txtnombre">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-2 col-form-label">Descripción</label>
                            <div class="col-10">
                                <input class="form-control" type="text" value="" id="txtdescripcion" name="txtdescripcion">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-2 col-form-label">Alerjenos</label>
                            <div class="col-10">
                                <input class="form-control" type="text" value="" id="txtalerjenos" name="txtalerjenos">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-2 col-form-label">Foto</label>
                            <div class="col-10">
                                <input class="form-control" type="file" value="" id="txtfoto" name="txtfoto" accept="image/*">
                                <div id="txtfotoPreview"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-2 col-form-label">Precio</label>
                            <div class="col-10">
                                <input class="form-control" type="text" value="" id="txtprecio" name="txtprecio">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-2 col-form-label">Destacado</label>
                            <div class="col-10">
                                <select class="form-control" name="txtdestacado" id="txtdestacado">
                                    <option value="1">Destacar</option>
                                    <option value="0">Sin destacar</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-2 col-form-label">Estatus</label>
                            <div class="col-10">
                                <select class="form-control" name="txtestatus" id="txtestatus">
                                    <option value="1">Mostrar</option>
                                    <option value="0">ocultar</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-2 col-form-label">Categoría</label>
                            <div class="col-10">
                                <select class="form-control" name="txtcategoria" id="txtcategoria">
                                    @foreach($categorias as $categoria)
                                    <option value="{{$categoria->id}}">{{$categoria->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @php
                            $ordenamiento = $productos->count() + 1;
                        @endphp
                        <div class="form-group row">
                            <label for="example-text-input" class="col-2 col-form-label">Ordenamiento</label>
                            <div class="col-10">
                                <select class="form-control" name="txtordenamiento" id="txtordenamiento">
                                    @for($i=1; $i <= $ordenamiento; $i++)
                                    <option value="{{$i}}">{{$i}}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                    </div>
                    @csrf
                </form>
            </div>
            <div class="modal-footer">
            <div class="invalid-feedback">cdfsd</div>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-success" id="btnguardar">Guardar</button>
            </div>
        </div>
    </div>
</div>


@endsection
@section('javascript')
<script>
$( document ).ready(function() {
    $("#btnguardar").click(function(){
        // $(this).attr("disabled", true);
        var frm = $(".kt-form");
        guardar(frm);
    });

    function guardar(formulario){
        $data = new FormData(formulario[0]);
        $url = formulario.attr("action");
        $(".invalid-feedback").html("");

        $.ajax({
            method: "POST",
            url: $url,
            data: $data,
            enctype: 'multipart/form-data',
            processData: false,  // tell jQuery not to process the data
            contentType: false   // tell jQuery not to set contentType
            })
            .done(function( data ) {
                console.log(data);
                if(data.error){
                    $(".invalid-feedback").show();
                    $(".invalid-feedback").html("<h5>" + data.mensaje+ "</h5>");
                }else{
                    window.location.href = data.url;
                }
                
            });
    }

    $("#txtfoto").change(function () {
        filePreview(this);
    });

    function filePreview(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                // $('#uploadForm + img').remove();
                $('#txtfotoPreview').html('<img src="'+e.target.result+'" width="200" height="200"/>');
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

});
</script>
@endsection