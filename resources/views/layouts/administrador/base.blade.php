<!DOCTYPE html>
<html lang="en">

    <!-- begin::Head -->
    <head>
        <base href="../../../">
        <meta charset="utf-8" />
        <title>La Carta</title>
        <meta name="description" content="Base form control examples">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!--begin::Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">

        <!--end::Fonts -->

        <!--begin::Global Theme Styles(used by all pages) -->
        <link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
@yield('css')
        <!--end::Global Theme Styles -->

        <!--begin::Layout Skins(used by all pages) -->

        <!--end::Layout Skins -->
        <link rel="shortcut icon" href="assets/media/logos/favicon.ico" />
    </head>

    <!-- end::Head -->

    <!-- begin::Body -->
    <body class="kt-page--loading-enabled kt-page--loading kt-header--fixed kt-header--minimize-topbar kt-header-mobile--fixed kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-subheader--enabled kt-subheader--transparent kt-page--loading">

        <!-- begin::Page loader -->

        <!-- end::Page Loader -->

        <!-- begin:: Page -->

        <!-- begin:: Header Mobile -->
        <div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
            <div class="kt-header-mobile__brand">
                <a class="kt-header-mobile__logo" href="?page=index&demo=demo5">
                    <img alt="Logo" src="assets/media/logos/logo-5-sm.png" />
                </a>
                <div class="kt-header-mobile__nav">
                    <div class="dropdown">
                        <button type="button" class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--md kt-svg-icon--success">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24" />
                                    <path d="M15.9956071,6 L9,6 C7.34314575,6 6,7.34314575 6,9 L6,15.9956071 C4.70185442,15.9316381 4,15.1706419 4,13.8181818 L4,6.18181818 C4,4.76751186 4.76751186,4 6.18181818,4 L13.8181818,4 C15.1706419,4 15.9316381,4.70185442 15.9956071,6 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                    <path d="M10.1818182,8 L17.8181818,8 C19.2324881,8 20,8.76751186 20,10.1818182 L20,17.8181818 C20,19.2324881 19.2324881,20 17.8181818,20 L10.1818182,20 C8.76751186,20 8,19.2324881 8,17.8181818 L8,10.1818182 C8,8.76751186 8.76751186,8 10.1818182,8 Z" fill="#000000" />
                                </g>
                            </svg> </button>
                        <div class="dropdown-menu dropdown-menu-fit dropdown-menu-md">
                            <ul class="kt-nav kt-nav--bold kt-nav--md-space kt-margin-t-20 kt-margin-b-20">
                                <li class="kt-nav__item">
                                    <a class="kt-nav__link active" href="#">
                                        <span class="kt-nav__link-icon"><i class="flaticon2-user"></i></span>
                                        <span class="kt-nav__link-text">Human Resources</span>
                                    </a>
                                </li>
                                <li class="kt-nav__item">
                                    <a class="kt-nav__link" href="#">
                                        <span class="kt-nav__link-icon"><i class="flaticon-feed"></i></span>
                                        <span class="kt-nav__link-text">Customer Relationship</span>
                                    </a>
                                </li>
                                <li class="kt-nav__item">
                                    <a class="kt-nav__link" href="#">
                                        <span class="kt-nav__link-icon"><i class="flaticon2-settings"></i></span>
                                        <span class="kt-nav__link-text">Order Processing</span>
                                    </a>
                                </li>
                                <li class="kt-nav__item">
                                    <a class="kt-nav__link" href="#">
                                        <span class="kt-nav__link-icon"><i class="flaticon2-chart2"></i></span>
                                        <span class="kt-nav__link-text">Accounting</span>
                                    </a>
                                </li>
                                <li class="kt-nav__separator"></li>
                                <li class="kt-nav__item">
                                    <a class="kt-nav__link" href="#">
                                        <span class="kt-nav__link-icon"><i class="flaticon-security"></i></span>
                                        <span class="kt-nav__link-text">Finance</span>
                                    </a>
                                </li>
                                <li class="kt-nav__item">
                                    <a class="kt-nav__link" href="#">
                                        <span class="kt-nav__link-icon"><i class="flaticon2-cup"></i></span>
                                        <span class="kt-nav__link-text">Administration</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-header-mobile__toolbar">
                <button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
                <button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more-1"></i></button>
            </div>
        </div>

        <!-- end:: Header Mobile -->
        <div class="kt-grid kt-grid--hor kt-grid--root">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">

                    <!-- begin:: Header -->
                    <div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed " data-ktheader-minimize="on">
                        <div class="kt-header__top">
                            <div class="kt-container ">

                                <!-- begin:: Brand -->
                                <div class="kt-header__brand   kt-grid__item" id="kt_header_brand">
                                    <div class="kt-header__brand-logo">
                                        <a href="index&demo=demo5.html">
                                            <img alt="Logo" src="assets/media/logos/logo-5.png" />
                                        </a>
                                    </div>

                                </div>

                                <!-- end:: Brand -->

                                <!-- begin:: Header Topbar -->
                                <div class="kt-header__topbar">
                                    <!--begin: User bar -->
                                    <div class="kt-header__topbar-item kt-header__topbar-item--user">
                                        <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,10px">
                                            <span class="kt-hidden kt-header__topbar-welcome">Hi,</span>
                                            <span class="kt-hidden kt-header__topbar-username">Nick</span>
                                            <img class="kt-hidden-" alt="Pic" src="assets/media/users/300_21.jpg" />
                                            <span class="kt-header__topbar-icon kt-header__topbar-icon--brand kt-hidden"><b>S</b></span>
                                        </div>
                                        <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">

                                            <!--begin: Head -->
                                            <div class="kt-user-card kt-user-card--skin-light kt-notification-item-padding-x">
                                                <div class="kt-user-card__avatar">
                                                    <img class="kt-hidden-" alt="Pic" src="assets/media/users/300_25.jpg" />

                                                    <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                                                    <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold kt-hidden">S</span>
                                                </div>
                                                <div class="kt-user-card__name">
                                                    Sean Stone
                                                </div>
                                                <div class="kt-user-card__badge">
                                                    <span class="btn btn-label-primary btn-sm btn-bold btn-font-md">23 messages</span>
                                                </div>
                                            </div>

                                            <!--end: Head -->

                                            <!--begin: Navigation -->
                                            <div class="kt-notification">
                                                <a href="custom/apps/user/profile-1/personal-information&demo=demo5.html" class="kt-notification__item">
                                                    <div class="kt-notification__item-icon">
                                                        <i class="flaticon2-calendar-3 kt-font-success"></i>
                                                    </div>
                                                    <div class="kt-notification__item-details">
                                                        <div class="kt-notification__item-title kt-font-bold">
                                                            My Profile
                                                        </div>
                                                        <div class="kt-notification__item-time">
                                                            Account settings and more
                                                        </div>
                                                    </div>
                                                </a>
                                                <a href="custom/apps/user/profile-3&demo=demo5.html" class="kt-notification__item">
                                                    <div class="kt-notification__item-icon">
                                                        <i class="flaticon2-mail kt-font-warning"></i>
                                                    </div>
                                                    <div class="kt-notification__item-details">
                                                        <div class="kt-notification__item-title kt-font-bold">
                                                            My Messages
                                                        </div>
                                                        <div class="kt-notification__item-time">
                                                            Inbox and tasks
                                                        </div>
                                                    </div>
                                                </a>
                                                <a href="custom/apps/user/profile-2&demo=demo5.html" class="kt-notification__item">
                                                    <div class="kt-notification__item-icon">
                                                        <i class="flaticon2-rocket-1 kt-font-danger"></i>
                                                    </div>
                                                    <div class="kt-notification__item-details">
                                                        <div class="kt-notification__item-title kt-font-bold">
                                                            My Activities
                                                        </div>
                                                        <div class="kt-notification__item-time">
                                                            Logs and notifications
                                                        </div>
                                                    </div>
                                                </a>
                                                <a href="custom/apps/user/profile-3&demo=demo5.html" class="kt-notification__item">
                                                    <div class="kt-notification__item-icon">
                                                        <i class="flaticon2-hourglass kt-font-brand"></i>
                                                    </div>
                                                    <div class="kt-notification__item-details">
                                                        <div class="kt-notification__item-title kt-font-bold">
                                                            My Tasks
                                                        </div>
                                                        <div class="kt-notification__item-time">
                                                            latest tasks and projects
                                                        </div>
                                                    </div>
                                                </a>
                                                <a href="custom/apps/user/profile-1/overview&demo=demo5.html" class="kt-notification__item">
                                                    <div class="kt-notification__item-icon">
                                                        <i class="flaticon2-cardiogram kt-font-warning"></i>
                                                    </div>
                                                    <div class="kt-notification__item-details">
                                                        <div class="kt-notification__item-title kt-font-bold">
                                                            Billing
                                                        </div>
                                                        <div class="kt-notification__item-time">
                                                            billing & statements <span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill kt-badge--rounded">2 pending</span>
                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="kt-notification__custom kt-space-between">
                                                    <a href="custom/user/login-v2&demo=demo5.html" target="_blank" class="btn btn-label btn-label-brand btn-sm btn-bold">Sign Out</a>
                                                    <a href="custom/user/login-v2&demo=demo5.html" target="_blank" class="btn btn-clean btn-sm btn-bold">Upgrade Plan</a>
                                                </div>
                                            </div>

                                            <!--end: Navigation -->
                                        </div>
                                    </div>

                                    <!--end: User bar -->
                                </div>

                                <!-- end:: Header Topbar -->
                            </div>
                        </div>
                        <div class="kt-header__bottom">
                            <div class="kt-container ">

                                <!-- begin: Header Menu -->
@include('layouts.administrador.menu')
                                <!-- end: Header Menu -->
                            </div>
                        </div>
                    </div>

                    <!-- end:: Header -->
                    <div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
                        <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

@include('layouts.administrador.breadcrumb')

                            <!-- begin:: Content -->
                            <div class="kt-container  kt-grid__item kt-grid__item--fluid">
                                <div class="row">
@yield('cuerpo')
                                </div>
                            </div>

                            <!-- end:: Content -->
                        </div>
                    </div>
@include('layouts.administrador.footer')
                </div>
            </div>
        </div>

        <!-- end:: Page -->

        <!-- begin::Scrolltop -->
        <div id="kt_scrolltop" class="kt-scrolltop">
            <i class="fa fa-arrow-up"></i>
        </div>

        <!-- end::Scrolltop -->

        <!-- begin::Global Config(global config for global JS sciprts) -->
        <script>
            var KTAppOptions = {
                "colors": {
                    "state": {
                        "brand": "#3d94fb",
                        "light": "#ffffff",
                        "dark": "#282a3c",
                        "primary": "#5867dd",
                        "success": "#34bfa3",
                        "info": "#3d94fb",
                        "warning": "#ffb822",
                        "danger": "#fd27eb"
                    },
                    "base": {
                        "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                        "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
                    }
                }
            };
        </script>

        <!-- end::Global Config -->

        <!--begin::Global Theme Bundle(used by all pages) -->
        <script src="assets/plugins/global/plugins.bundle.js" type="text/javascript"></script>
        <script src="assets/js/scripts.bundle.js" type="text/javascript"></script>
@yield('javascript')

        <!--end::Global Theme Bundle -->
    </body>

    <!-- end::Body -->
</html>